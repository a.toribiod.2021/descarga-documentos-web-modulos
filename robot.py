import urllib.request


class Robot:

    def __init__(self, url):
        self.url = url
        self.content = None

    def retrieve(self):
        if self.content is None:
            print("Descargando", self.url)
            self.content = urllib.request.urlopen(self.url).read()

    def show(self):
        self.retrieve()
        print(self.content.decode("utf-8"))

    def content(self):
        self.retrieve()
        return self.content.decode("utf-8")
