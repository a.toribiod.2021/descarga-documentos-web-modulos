from robot import Robot
from cache import Cache

# Crear robots
robot1 = Robot('https://cursosweb.github.io/')
robot2 = Robot('https://www.aulavirtual.urjc.es/moodle/')

# Usar métodos de Robot
robot1.retrieve()
robot1.show()
print(robot2.content)

# Crear caches
cache1 = Cache()
cache2 = Cache()

# Usar métodos de Cache
cache1.retrieve('https://cursosweb.github.io/')
cache1.show('https://cursosweb.github.io/')
cache2.retrieve('https://cursosweb.github.io/')
cache2.retrieve('https://www.aulavirtual.urjc.es/moodle/')
cache2.show_all()
print(cache1.content('https://www.aulavirtual.urjc.es/moodle/'))
