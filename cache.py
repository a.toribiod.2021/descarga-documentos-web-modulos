import urllib.request


class Cache:
    def __init__(self):
        self.cache = {}

    def retrieve(self, url):
        if url not in self.cache:
            with urllib.request.urlopen(url) as response:
                self.cache[url] = response.read()

    def show(self, url):
        self.retrieve(url)
        print(self.cache[url].decode('utf-8'))

    def show_all(self):
        for url in self.cache:
            print(url)

    def content(self, url):
        self.retrieve(url)
        return self.cache[url].decode('utf-8')
